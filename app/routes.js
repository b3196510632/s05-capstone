const { exchangeRates } = require("../src/util.js");

module.exports = (app) => {
  app.get("/", (req, res) => {
    return res.send({ data: {} });
  });

  app.get("/rates", (req, res) => {
    return res.send({
      rates: exchangeRates,
    });
  });

  app.post("/currency", (req, res) => {
    if (
      !req.body.hasOwnProperty("name") ||
      typeof req.body.name !== "string" ||
      req.body.name.length === 0
    ) {
      return res.status(400).send({
        error: "Bad Request: NAME is invalid or missing",
      });
    }

    if (
      !req.body.hasOwnProperty("ex") ||
      typeof req.body.ex !== "object" ||
      req.body.ex.length === 0
    ) {
      return res.status(400).send({
        error: "Bad Request: EX is invalid or missing",
      });
    }

    if (
      !req.body.hasOwnProperty("alias") ||
      typeof req.body.alias !== "string" ||
      req.body.alias.length === 0
    ) {
      return res.status(400).send({
        error: "Bad Request: ALIAS is invalid or missing",
      });
    }

    const alias = Object.keys(exchangeRates).map((currency) => currency);

    if (alias.includes(req.body.alias)) {
      return res.status(400).send({
        error: "Bad Request: ALIAS already exist",
      });
    }

    return res.status(200).send({
      data: req.body,
    });
  });
};
